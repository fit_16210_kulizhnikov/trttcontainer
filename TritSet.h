//
// Created by Savely on 09.12.2017.
//


#ifndef TRITCONTAINER_TRITSET_H
#define TRITCONTAINER_TRITSET_H

#include <iostream>

#include <map>
#include <unordered_map>

typedef unsigned int uint;

enum Trit{  False = -1,
            Unknown = 0,
            True = 1};

class TritWorker;
class TritIterator;


class TritSet {
private:
    friend class TritWorker;
    Trit getTrit(int);
    void setTrit(int,Trit);
public:
    explicit TritSet(int);

    uint *container;
    int size;

    TritSet& operator= (const TritSet&);
    TritSet operator& (TritSet&);
    TritSet operator| (TritSet&);
    TritSet operator! ();

    TritWorker operator[](int);

    Trit logicOperation(Trit,Trit,int);
    std::unordered_map<Trit,int> cardinality();
    int cardinality(Trit);
    int capacity();
    int length();
    void shrink();
    void trim(int);

    TritIterator begin();
    TritIterator end();

    std::map<Trit , int> tritToInt ={{True,1},{False,-1},{Unknown,0}};

    static const int ZERO = 0;
    static const int ONE = 1;
    static const int UNONE = -1;
    static const int DOUBLER = 2;
    static const int SIZEOFBYTE = 8;
    static const int ONE_ONE = 3;
    static const int ANDOPERATION = 1;
    static const int OROPERATION = 2;
    static const int NOTOPERATION = 3;
};


class TritWorker{
private:
    std::map<Trit , int> tritToInt ={{True,1},{False,-1},{Unknown,0}};
    int index;
    TritSet& tritSet;
public:
    TritWorker(int index, TritSet &tritSet);

    TritWorker&operator=(Trit);
    TritWorker&operator=(TritWorker);
    bool operator==(TritWorker);
    int showTrit();

    friend std::ostream&operator<<(std::ostream& os,const TritWorker& tw);
};

class TritIterator{
private:
    TritSet& tritSet;
    int index;
public:
    TritIterator(TritSet&,int);
    TritWorker operator*();
    TritIterator& operator++();
    TritIterator& operator++(int);

    bool operator==(TritIterator);
    bool operator!=(TritIterator);
    int getIndex();
};



#endif //TRITCONTAINER_TRITSET_H
