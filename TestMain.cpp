//
// Created by Savely on 05.01.2018.
//

#include <gtest/gtest.h>
#include <iostream>
#include "TritSet.h"


//Проверка TritSet



int main(int argc, char** argv) {
{
    std::cout<<"---------Вставка и Доставание элемента---------------\n";

//Вставка и Доставание элемента
    TritSet A(5);
    A[0] = True;
    EXPECT_TRUE(A[0].showTrit() == 1);
    A[2] = Unknown;
    EXPECT_TRUE(A[2].showTrit() == 0);
    A[3] = True;
    EXPECT_TRUE(A[3].showTrit() == 1);
    A[4] = False;
    EXPECT_TRUE(A[4].showTrit() == -1);
//При обращении к элементу когда index>size возвращается Unknown;
    EXPECT_TRUE(A[10].showTrit() == 0);
}
//Количество тритов, количество тритов после удаления блока
{
    std::cout<<"---------Количество тритов---------------\n";
    TritSet A(20);
    A[0] = True;
    A[1] = False;
    A[2] = False;
    EXPECT_EQ(1, A.cardinality(True));
    EXPECT_EQ(2, A.cardinality(False));
    EXPECT_EQ(0, A.cardinality(Unknown));
    A[3] = Unknown;
    A[4] = False;
    EXPECT_EQ(1, A.cardinality(Unknown));
}//----------------------------------------------------НЕ РОБИТ
{
    std::cout<<"----------количество тритов "
            "после удаления блока--------------\n";
    TritSet E(20);
    E[0] = True;
    E[1] = False;
    E[3] = True;
    E[7] = False;
    E[14] = True;
    E[18] = False;
    E[19] = True ;
    E.trim(12);
    EXPECT_EQ(2, E.cardinality(True));
    EXPECT_EQ(2, E.cardinality(False));
    EXPECT_EQ(4, E.cardinality(Unknown));
    E.trim(4);
    EXPECT_EQ(2, E.cardinality(True));
    EXPECT_EQ(1, E.cardinality(False));
    EXPECT_EQ(2, E.cardinality(Unknown));
}

//Проверка памяти
{
    std::cout<<"-----------Проверка памяти-------------\n";
    TritSet A(1000);
    int allocLength = A.capacity();
    ASSERT_LT(allocLength , 1000);
    A[10000] = Unknown;
    ASSERT_EQ(allocLength , A.capacity());
    A[10000] = True;
    ASSERT_LT(allocLength , A.capacity());
}
//Операторы Логики, изменение размера после операторов логики
{
    std::cout<<"-----------Операторы Логики- &&&&------------\n";
    TritSet A(9);
    A[0] = True;
    A[1] = True;
    A[2] = True;
    A[3] = False;
    A[4] = False;
    A[5] = False;
    TritSet B(9);
    B[0] = True;
    B[1] = False;
    B[3] = True;
    B[4] = False;
    B[6] = True;
    B[7] = False;
    A=A&B;
    EXPECT_EQ(1, A[0].showTrit());
    EXPECT_EQ(-1, A[1].showTrit());
    EXPECT_EQ(0, A[2].showTrit());
    EXPECT_EQ(-1, A[3].showTrit());
    EXPECT_EQ(-1, A[4].showTrit());
    EXPECT_EQ(-1, A[5].showTrit());
    EXPECT_EQ(0, A[6].showTrit());
    EXPECT_EQ(-1, A[7].showTrit());
    EXPECT_EQ(0, A[8].showTrit());
}
{
    std::cout<<"-----------|||||||-------------\n";
    TritSet A(9);
    A[0] = True;
    A[1] = True;
    A[2] = True;
    A[3] = False;
    A[4] = False;
    A[5] = False;
    TritSet B(9);
    B[0] = True;
    B[1] = False;
    B[3] = True;
    B[4] = False;
    B[6] = True;
    B[7] = False;
    A=A|B;
    EXPECT_EQ(1, A[0].showTrit());
    EXPECT_EQ(1, A[1].showTrit());
    EXPECT_EQ(1, A[2].showTrit());
    EXPECT_EQ(1, A[3].showTrit());
    EXPECT_EQ(-1, A[4].showTrit());
    EXPECT_EQ(0, A[5].showTrit());
    EXPECT_EQ(1, A[6].showTrit());
    EXPECT_EQ(0, A[7].showTrit());
    EXPECT_EQ(0, A[8].showTrit());
}
{
    std::cout<<"------------!!!!!!------------\n";
    TritSet A(3);
    A[0] = True;
    A[1] = False;
    A=!A;
    EXPECT_EQ(-1, A[0].showTrit());
    EXPECT_EQ(1, A[1].showTrit());
    EXPECT_EQ(0, A[2].showTrit());
}

{
    std::cout<<"-----------изменение"
            "размера после операторов логики---&&----------\n";
    TritSet A(1000);
    TritSet B(2000);
    A[100] = True;
    B[100] = True;
    A[20] = False;
    TritSet C(1000);
    C = (A & B);
    ASSERT_EQ(C.capacity() , B.capacity());
}

{
    std::cout<<"-----------изменение"
            "размера после операторов логики------||-------\n";
    TritSet A(1000);
    TritSet B(2000);
    A[100] = True;
    B[100] = False;
    A[20] = False;
    TritSet C(1000);
    C = (A | B);
    ASSERT_EQ(C.capacity() , B.capacity());
}


//Оператор = и конструктор копирования
{
    std::cout<<"----------Оператор = и конструктор копирования--------------\n";
    TritSet A(3);
    A[0] = True;
    A[1] = False;
    TritSet B(2);
    B = A;
    EXPECT_EQ(1, B.capacity());
    EXPECT_EQ(1, B[0].showTrit());
    EXPECT_EQ(-1, B[1].showTrit());
    EXPECT_EQ(0, B[2].showTrit());
}

//Длина, Начало, Конец
{
    std::cout<<"---------Длина---------------\n";
    TritSet A(20);
    A[4] = True;
    EXPECT_EQ(5 , A.length());
    A[2] = False;
    EXPECT_EQ(5 , A.length());
    A[7] = Unknown;
    EXPECT_EQ(5 , A.length());
}

{
    std::cout<<"---------Начало, Конец---------------\n";
    TritSet A(10);
    A[5] = True;
    A[7] = False;
    EXPECT_EQ(0, A.begin().getIndex() );
    EXPECT_EQ(7, A.end().getIndex() );
}

//Удаление
{
    std::cout<<"----------Удаление---trim-----------\n";
    TritSet A(9);
    EXPECT_EQ(3, A.capacity());
    A[3] = True;
    A[5] = False;
    A.trim(4);
    EXPECT_EQ(1, A.capacity());
    EXPECT_EQ(0, A[5].showTrit());
    EXPECT_EQ(0, A[6].showTrit());
}

{
    std::cout<<"---------Удаление----shrink-----------\n";
    TritSet A(9);
    EXPECT_EQ(3, A.capacity());
    A[3] = True;
    A[5] = False;
    A.shrink();
    EXPECT_EQ(2, A.capacity());
}
    return 0;

}