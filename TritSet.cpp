//
// Created by Savely on 09.12.2017.
//

#include "TritSet.h"

TritSet::TritSet(int size) : size(size) {
    container = new uint[capacity()/sizeof(uint)+ONE];
    for (int i = 0; i < capacity()/sizeof(uint)+ONE; ++i) {
        container[i]=ZERO;
    }
}


int TritSet::capacity() {
    if (size*DOUBLER%SIZEOFBYTE==0){
        return size*DOUBLER/SIZEOFBYTE;
    }
    return size*DOUBLER/SIZEOFBYTE+1;
}

int TritSet::cardinality(Trit trit) {
    int res = ZERO;
    Trit tmp;
    for (int i = 0; i <= length(); ++i) {
        tmp = getTrit(i);
        if(tritToInt[tmp]==tritToInt[trit]){
            res++;
        }
    }
    return res;
}

TritWorker TritSet::operator[](int numberOfTrit) {
    return TritWorker(numberOfTrit*DOUBLER,*this);
}


Trit TritSet::getTrit(int numberOfTrit) {
    int numberOfBytes = (numberOfTrit*DOUBLER/(sizeof(uint)*SIZEOFBYTE));
    int numberOfPosition = ((numberOfTrit*DOUBLER)%(sizeof(uint)*SIZEOFBYTE));
    int elementOfContainer = container[numberOfBytes];
    elementOfContainer>>=numberOfPosition;
    elementOfContainer&=ONE_ONE;
    switch (elementOfContainer){
        case ONE :
            return False;
        case ZERO :
            return Unknown;
        case ONE_ONE :
            return True;
        default:
            return Unknown;
    }
}

void TritSet::setTrit(int index, Trit trit) {
    int numberOfBytes = (index/(sizeof(uint)*SIZEOFBYTE));
    int numberOfPosition = (index%(sizeof(uint)*SIZEOFBYTE));
    int firstCoordinate = ONE<<numberOfPosition;
    int secondCoordinate = ONE<<(numberOfPosition+ONE);
    switch (tritToInt[trit]){
        case ONE:{
            container[numberOfBytes]|=(firstCoordinate+secondCoordinate);
            break;
        }
        case UNONE:{
            container[numberOfBytes]|=(firstCoordinate);
            container[numberOfBytes]&=~(secondCoordinate);
            break;
        }
        default:{
            container[numberOfBytes]&=~(firstCoordinate);
            container[numberOfBytes]&=~(secondCoordinate);
            break;
        }
    }
}

TritSet &TritSet::operator=(const TritSet &tritOut) {
    if(this == &tritOut)
    {
        return *this;
    }
    (*this).size = tritOut.size;
    delete (*this).container;
    (*this).container = new uint[capacity()/sizeof(uint)+1];
    for (int i = 0; i < capacity()/sizeof(uint)+ONE; ++i) {
        this->container[i]=tritOut.container[i];
    }
    return *this;
}

int TritSet::length() {
    int i= sizeof(uint)*SIZEOFBYTE;
    int j;
    bool flag = false;
    for (j = size*DOUBLER/SIZEOFBYTE/sizeof(uint); j >= ZERO; --j) {
        i = sizeof(uint)*SIZEOFBYTE;
        while(((container[j] >> (i-ONE)) & ONE)!=ONE){
            i--;
            if(i==UNONE){
                break;
            }
        }
        if(i!=UNONE){
            flag = true;
            break;
        }
    }
    if(flag){
        return i/DOUBLER+j*(sizeof(uint)*SIZEOFBYTE/DOUBLER);
    }
    return 0;
}

void TritSet::shrink() {
    int newSize = length()*DOUBLER/SIZEOFBYTE/ sizeof(uint)+ONE;
    auto *newContainer = new uint[newSize];
    memcpy(newContainer,container,newSize*sizeof(uint));
    size = length();
    delete[] container;
    container = newContainer;
}

void TritSet::trim(int index) {
    int newSize = index*DOUBLER/SIZEOFBYTE/sizeof(uint);
    auto *newContainer = new uint[newSize];
    memcpy(newContainer,container,index*sizeof(uint));
    int lastSetUint = index*2/(sizeof(uint)*SIZEOFBYTE);
    int lastBit = index*2 - lastSetUint*(sizeof(uint)*SIZEOFBYTE);
    for (int i = lastBit; i < (sizeof(uint)*SIZEOFBYTE); ++i) {
        newContainer[lastSetUint]&=(~(1<<i));
    }
    size = index;
    delete[] container;
    container = newContainer;
}

std::unordered_map<Trit, int> TritSet::cardinality() {
    std::unordered_map<Trit, int> res{{True,ZERO},{Unknown,ZERO},{False,ZERO}};
    res[True] = cardinality(True);
    res[False] = cardinality(False);
    res[Unknown] = cardinality(Unknown);
    return res;
}

TritSet TritSet::operator&(TritSet & rightSide) {
    TritSet &leftSide = *this;
    int maxSize = leftSide.size > rightSide.size
                   ? leftSide.size :rightSide.size;
    TritSet result(maxSize);
    for (int i = 0; i < maxSize; ++i) {
        result[i] = logicOperation(leftSide.getTrit(i),rightSide.getTrit(i)
                ,ANDOPERATION);
    }
    return result;
}

TritSet TritSet::operator|(TritSet &rightSide) {
    TritSet &leftSide = *this;
    int maxSize = leftSide.size > rightSide.size
                  ? leftSide.size :rightSide.size;
    TritSet result(maxSize);
    for (int i = 0; i < maxSize; ++i) {
        result[i] = logicOperation(leftSide.getTrit(i),
                                        rightSide.getTrit(i),OROPERATION);
    }
    return result;
}

TritSet TritSet::operator!() {
    TritSet &leftSide = *this;
    TritSet result(leftSide.size);
    for (int i = 0; i < leftSide.size; ++i) {
        result[i] = logicOperation(leftSide.getTrit(i),Unknown,NOTOPERATION);
    }
    return result;
}

Trit TritSet::logicOperation(Trit leftSide, Trit rightSide, int logicType) {
    switch(logicType){
        case ANDOPERATION: //and
            if((tritToInt[leftSide] == tritToInt[False]) ||
                    (tritToInt[rightSide] == tritToInt[False])) {
                return False;
            }
            if((tritToInt[leftSide] == tritToInt[True]) &&
                    (tritToInt[rightSide] == tritToInt[True])) {
                return True;
            }
            break;
        case OROPERATION: // or
            if((tritToInt[leftSide] == tritToInt[False]) &&
               (tritToInt[rightSide] == tritToInt[False])) {
                return False;
            }
            if((tritToInt[leftSide] == tritToInt[True]) ||
               (tritToInt[rightSide] == tritToInt[True])) {
                return True;
            }
            break;
        case NOTOPERATION: // not
            if(tritToInt[leftSide] == tritToInt[True])
                return False;
            if(tritToInt[leftSide] == tritToInt[False])
                return True;
            break;
        default:
            return Unknown;
    }
    return Unknown;
}

TritIterator TritSet::begin() {
    return TritIterator(*this, ZERO);
}

TritIterator TritSet::end() {
    return TritIterator(*this,this->length());
}


TritWorker::TritWorker(int index,
                       TritSet &tritSet) : index(index),tritSet(tritSet) {}

TritWorker &TritWorker::operator=(const Trit tritFrom) {
    if((*this).tritSet.size<(*this).index/TritSet::DOUBLER)
    {
        if(tritToInt[tritFrom] != TritSet::ZERO){
            int prevSize = (*this).tritSet.size;
            (*this).tritSet.size = ((*this).index/TritSet::DOUBLER);
            uint *buf = this->tritSet.container;
            this->tritSet.container = new uint[this->
                    tritSet.size*TritSet::DOUBLER /TritSet::SIZEOFBYTE/
                                                                sizeof(uint)];
            for (int i = 0; i < this->
                    tritSet.size*TritSet::DOUBLER/TritSet::SIZEOFBYTE/
                                                            sizeof(uint); ++i) {
                if(i<prevSize){
                    this->tritSet.container[i] = buf[i];
                } else{
                    this->tritSet.container[i] = TritSet::ZERO;
                }
            }
            delete [] buf;
            (*this).tritSet.setTrit(index,tritFrom);
            return (*this);
        }
        return (*this);
    }
    (*this).tritSet.setTrit(index,tritFrom);
    return (*this);
}

int TritWorker::showTrit() {
    int numberOfBytes = (index/(sizeof(uint)*TritSet::SIZEOFBYTE));
    int numberOfPosition = (index%(sizeof(uint)*TritSet::SIZEOFBYTE));
    int elementOfContainer = tritSet.container[numberOfBytes];
    elementOfContainer>>=numberOfPosition;
    elementOfContainer&=TritSet::ONE_ONE;
    switch (elementOfContainer){
        case TritSet::ONE :
            return -1;
        case TritSet::ZERO :
            return 0;
        case TritSet::ONE_ONE :
            return 1;
        default:
            return 0;
    }
}

std::ostream &operator<<(std::ostream &os, const TritWorker &tw) {
    if(tw.index/TritSet::DOUBLER> tw.tritSet.size)
    {
        os<<"Unknown"<<std::endl;
        return os;
    }
    int numberOfBytes = (tw.index/(sizeof(uint)*TritSet::SIZEOFBYTE));
    int numberOfPosition = (tw.index%(sizeof(uint)*TritSet::SIZEOFBYTE));
    int elementOfContainer = tw.tritSet.container[numberOfBytes];
    elementOfContainer>>=numberOfPosition;
    elementOfContainer&=TritSet::ONE_ONE;
    switch (elementOfContainer){
        case TritSet::ONE :
            os<<"False"<<std::endl;
            return os;
        case TritSet::ZERO:
            os<<"Unknown"<<std::endl;
            return os;
        case TritSet::ONE_ONE:
            os<<"True"<<std::endl;
            return os;
        default:
            os<<"Unknown"<<std::endl;
            return os;
    }
}

TritWorker &TritWorker::operator=(TritWorker tw) {
    if(((*this).tritSet.size<(*this).index/TritSet::DOUBLER)||
            (tw.tritSet.size<tw.index/TritSet::DOUBLER))
    {
        return (*this);
    }
    (*this).tritSet.setTrit(index,tw.tritSet.getTrit(tw.index/TritSet::DOUBLER));
    return (*this);
}

bool TritWorker::operator==(const TritWorker rightSide) {
    return this->tritSet.tritToInt[this->tritSet.getTrit(this->index)]==
            rightSide.tritSet.tritToInt[rightSide.tritSet.getTrit(rightSide.index)];
}

TritIterator::TritIterator(TritSet &tritSet, int index):tritSet(tritSet),
                                                        index(index){}

TritWorker TritIterator::operator*() {
    return this->tritSet[this->index];
}

TritIterator &TritIterator::operator++() {
    (*this).index+=TritSet::DOUBLER;
    return *this;;
}

TritIterator &TritIterator::operator++(int index) {
    (*this).index += index+TritSet::DOUBLER;
    return *this;
}

bool TritIterator::operator==(TritIterator rightSide) {
    return this->index == rightSide.index;
}

bool TritIterator::operator!=(TritIterator rightSide) {
    return this->index != rightSide.index;
}

int TritIterator::getIndex() {
    return (*this).index;
}
