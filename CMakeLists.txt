cmake_minimum_required(VERSION 3.8)
project(TritContainer)

include_directories(lib/googletest/include lib/googletest)


set(CMAKE_CXX_STANDARD 17)

add_definitions (-Wall)


add_subdirectory(lib/googletest)


set(SOURCE_FILES main.cpp TritSet.cpp TritSet.h TestMain.cpp TestMain.h)
set(UNIT_TESTS TestMain.cpp TritSet.cpp TritSet.h)


add_executable(TritContainer ${SOURCE_FILES})
add_executable(tests ${UNIT_TESTS})

target_link_libraries(tests gtest gtest_main)